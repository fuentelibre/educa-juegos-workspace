# Mariposa 68 - Butterfly 68

https://butterfly68-icarito.hashbase.io/

**Authors**: Colectivo Intamalase Collective

**Description**: _La obra es una ofrenda al río pidiéndole con humildad **la cura para el sútil sagrado femenino**, compuesto por los brillos de la vida que es albergada a su alrededor._ The work is an offering to the river, humbly asking for healing of the subtle sacred feminity, composed by the shines of the life sheltered around it.

 ### Butterfly 68 / Mariposa 68: _Diaethria Clymena_

Río Tambopata, Madre de Dios, Amazonian rainforest

Photos and audio taken at Tambopata River, April 2018

* Creation: Madre Naturaleza / Mother Nature
* Photography: Laura Vargas & Sebastian Silva
* Audio: Narowé Silva Vargas & Laura Vargas
* Producer: Laura Vargas & Inti Silva Vargas
* Script: Tao Silva Vargas & Laura Vargas
* Programming: Sebastian Silva & Narowé Silva Vargas


License: [**AGPL v3**](LICENSE)

