14 Estoy en el Tao. En él, Soy Uno, junto con los demás admirables.

13 No lucho contra nadie por nada y, por lo tanto, soy invulnerable.

12 Concentro mis esfuerzos en tener suficiente comida y pocas cosas.

11 La utilidad de los objetos emana del espacio vacío que hay en ellos.

10 Gobierno con amor y soy parte integrante del Absoluto que es Tao.

9 No me excedo nunca con nada.

8 Vivo como el agua, ella sirve a todos los seres sin exigir nada para sí.

7 Me pongo detrás de los otros, así no les estorbo y puedo guiarlos a Tao.

6 Tao es el fundamento sobre el cual el mundo material existe.

5 Imparcial con los demás, sigo siempre el principio de la no interferencia.

4 Tao es la Luz brillante, origen de todo. Tao es primordial.

3 No elogio a supuestos escogidos y no exhibo tesoros materiales.

2 Meditando continuamente me desapego de todo en la Tierra.

1 Tao y Su Creación son, en sustancia, Uno. Ambos son sagrados.  