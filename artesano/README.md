# Programación para Todos

*Bienvenido al sendero de la Programación de Computadoras.*

| **¿Qué es la Programación?**
| ---
| Programar es escribir texto en un **Lenguaje de Programacion** que la computadora puede interpretar y obedecer. Hay computadoras pequeñas tales como celulares y también más grandes como los servidores de Internet; Todas ellas funcionan de forma similar y requieren de programas de *Software* escritos por programadores para ser útiles. Una vez que un programa ha sido escrito puede ejecutarse infinitas veces.

Si lees estas líneas, seguramente estás contemplando aprender las técnicas del oficio. En este libro-taller consideramos la programación como una técnica de artesanía que se asemeja más a *escribir guiones*, que a hacer matemáticas. Estos guiones se desenvuelven entonces sobre un maravilloso recurso interactivo llamado *computadora*.

Aunque programar está al alcance de todos, tal vez no todos se sentirán inclinados a dedicar el esfuerzo. Este libro-taller propone que todos deberían intentarlo, porque:

  * El mundo necesita programadores.
  * Programar te hará libre.
  * Aprenderás a pensar en forma clara y ordenada.

