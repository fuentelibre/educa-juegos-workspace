### Hipótesis (método científico)

Para otros usos de este término, véase Hipótesis.

Una **hipótesis** (del [latín](https://es.wikipedia.org/wiki/Lat%C3%ADn)  hypothĕsis y este del [griego](https://es.wikipedia.org/wiki/Idioma_griego) ὑπόθεσις) es una «suposición de algoposible o imposible para sacar de ello una o más consecuencias».
Es una idea que puede no ser verdadera, basada en información previa. Su valor reside en la capacidad para establecer más relaciones entre los hechos y explicar por qué se producen. Normalmente se plantean primero las razones claras por las que uno cree que algo es posible. Y finalmente ponemos: en conclusión. Este método se usa en el método científico, para luego comprobar las hipótesis a través de los experimentos.



Una hipótesis científica** es una [propocición](https://es.wikipedia.org/wiki/Suposici%C3%B3n) aceptable que ha sido formulada a través de la recolección de información y datos, aunque no esté confirmada, sirve para responder de forma alternativa a un problema con [base científica](https://es.wikipedia.org/wiki/Investigaci%C3%B3n_cient%C3%ADfica).

Una hipótesis puede usarse como una propuesta provisional que no se pretende demostrar estrictamente, o puede ser una [predicción](https://es.wikipedia.org/wiki/Predicci%C3%B3n)