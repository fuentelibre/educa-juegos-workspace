# ballet
El **ballet**, reconocido por la Real Academia Española (RAE) como palabra francesa, proveniente del italiano balletto, diminutivo de ballo, que significa ‘baile’, danza académica o danza clásica es una forma concreta de danza y también el nombre de la técnica correspondiente. Según las épocas, los países o las corrientes y el espectáculo, esta expresión artística puede incluir: danza, mímica, y teatro (de orquesta y coral), personas y maquinaria.
Pintura de ballet de Edgar Degas (Musée d'Orsay).

El ballet clásico o danza clásica es una forma de danza cuyos movimientos se basan en el control total y absoluto del cuerpo, el cual se debe enseñar desde temprana edad. Se recomienda iniciar los estudios de esta danza clásica a los tres o cuatro años, ya que el ballet es una disciplina que requiere concentración y capacidad para el esfuerzo como actitud y forma de vida. A diferencia de otras danzas, en el ballet cada paso está codificado. Participan invariablemente las manos, brazos, tronco, cabeza, pies, rodillas, todo el cuerpo en una conjunción simultánea de dinámica muscular y mental que debe expresarse en total armonía de movimientos.

También se utiliza el término ballet para designar una pieza musical compuesta, a propósito, para ser interpretada por medio de la danza. El ballet es una de las artes escénicas.


# El Jurásico
El Jurásico es una división de la escala temporal geológica que pertenece a la Era Mesozoica; dentro de esta, el Jurásico ocupa el segundo lugar siguiendo al Triásico y precediendo al Cretácico. Comenzó hace 201 millones de años y acabó hace 145 millones de años.2​ Debe su nombre a la cadena montañosa del Jura, en los Alpes, lugar donde el geólogo prusiano Alexander von Humboldt identificó este sistema en 1795.

Como ocurre con la mayoría de las eras geológicas, las fechas exactas de inicio y fin de este período, como en los demás sistemas, son convencionales, conforme a ciertos criterios que se establecen para su datación, por lo que se admite algún error de magnitud en miles o millones de años.

Este período se caracteriza por la hegemonía de los grandes dinosaurios y por la escisión de Pangea en los continentes Laurasia y Gondwana. De este último se escindió Australia (en el jurásico superior y principios de cretáceo), del mismo modo que Laurasia se dividió en Norteamérica y Eurasia, dando origen a nuevas especies de mamíferos (véase Teriología).

El **Jurásico** se divide en Inferior, Medio y Superior, también conocidos como Lias, Dogger y Malm.

Índice

    1 Subdivisiones
    2 Paleogeografía
        2.1 La fragmentación de Pangea
    3 Vegetación
    4 Fauna
        4.1 Vertebrados terrestres
        4.2 Vertebrados aéreos
        4.3 Vertebrados acuáticos
    5 Véase también
    6 Notas y referencias
    7 Bibliografía
    8 Enlaces externos

Subdivisiones

La Comisión Internacional de Estratigrafía2​ reconoce tres épocas/series y once edades/pisos del Jurásico, distribuidos en orden de los más recientes a los más antiguos como sigue:
Estratos del Jurásico en Italia.
Era
Eratema 	Periodo
Sistema 	Época
Serie 	Edad
Piso 	Eventos relevantes 	Inicio, en
millones
de años
Mesozoico
Cretácico 		145,0
Jurásico 	Superior / Tardío 	Titoniense 	Son comunes gimnospermas (especialmente coníferas, Bennettitales y cicadas) y helechos. Muchos tipos de dinosaurios, como saurópodos, carnosaurios, y estegosaurios. Los mamíferos son comunes pero pequeños. Primeras aves y lagartos. Ictiosaurios y plesiosaurios se diversifican. Bivalvos, ammonites y belemnites abundan. Los erizos de mar son muy comunes, junto con crinoides, estrellas de mar, esponjas, y braquiópodos terebratúlidos y rinconélidos. Ruptura de Pangea en Gondwana y Laurasia. 	152,1±0,9
Kimmeridgiense 	157,3±1,0
Oxfordiense 	163,5±1,0
Medio 	Calloviense 	166,1±1,2
Bathoniense 	Clavo dorado.svg168,3±1,3
Bajociense 	Clavo dorado.svg170,3±1,4
Aaleniense 	Clavo dorado.svg174,1±1,0
Inferior / Temprano 	Toarciense 	182,7±0,7
Pliensbachiense 	Clavo dorado.svg190,8±1,0
Sinemuriense 	Clavo dorado.svg199,3±0,3
Hettangiense 	Clavo dorado.svg 201,3±0,2
Triásico 		252,17±0,06
Paleogeografía

El nivel del mar experimentó cambios menores durante el Jurásico Inferior. En el Jurásico Superior experimentó oscilaciones más rápidas y una elevación que ocasionó la inundación de grandes áreas de América del Norte y Europa. En el Jurásico hay dos provincias biogeográficas en Europa (Tetis al sur y Boreal al norte). Los arrecifes de coral se restringieron en su mayor parte a la provincia de Tetis. La transición entre las dos provincias se daba a la altura de la península ibérica. Las plantas de climas cálidos ocuparon un amplio cinturón que se extendió hasta 60º de latitud. Tanto la flora de Gondwana al sur como al norte de Siberia incluía grupos de helechos (sus parientes modernos no toleran las heladas).

El registro geológico jurásico es bueno en el oeste de Europa, donde extensas secuencias marinas indican un tiempo donde gran parte del continente estaba sumergido bajo mares tropicales poco profundos; por su fama destaca el Patrimonio Mundial de la Costa Jurásica y los lagerstätten de Holzmaden y Solnhofen.
La fragmentación de Pangea

El más espectacular desarrollo geográfico de la Era Mesozoica fue la fragmentación de Pangea, un proceso de rifting que comenzó en la región de Tetis, siguiendo la vieja sutura hercínica. El océano Tetis avanzó formando un estrecho y profundo brazo oceánico que separó Europa de África. El rift se propagó hacia el norte y finalmente hacia el sur comenzando a separar Sudamérica y África. Finalmente Pangea dio lugar al supercontinente septentrional de Laurasia y al supercontinente meridional de Gondwana; el golfo de México se abrió en el nuevo rift entre Norteamérica y lo que ahora es la península de Yucatán de México.

El rift que formó el Atlántico tuvo otra consecuencia importante, la extensión produjo fallas normales entre África y los continentes norteños y las zonas afectadas por tales fallas se hundieron, de forma que el agua que entraba periódicamente desde el océano Tetis comenzó a evaporarse. Dichas evaporitas se localizan ahora a ambos lados del Atlántico: España, Marruecos, Terranova, etc. Durante el Jurásico medio y superior un brazo de rift se desplazó entre Norteamérica y Sudamérica, dando origen al golfo de México; otro dio lugar a la apertura del golfo de Vizcaya. El océano Atlántico Norte era relativamente estrecho, y el Océano Atlántico Sur no se formó hasta el Cretácico, cuando la propia Gondwana se fragmentó.
Vegetación
Fósil de Ginkgo biloba.

Los paisajes del Jurásico fueron más ricos en vegetación que los del Triásico, especialmente en latitudes altas. El calor y el clima húmedo permitieron que las junglas, selvas y bosques formaran parte de gran cantidad de paisajes jurásicos. Los bosques se empiezan a extender por toda la superficie terrestre y destacan familias como las coníferas similares a los pinos y las araucarias acompañadas de diferentes tipos de helechos y palmeras. Además se hacían presente los ginkgos y los equisetos. Aún no aparecen en este periodo las plantas con inflorescencias. La distribución diferencial de la flora constituye un fiel reflejo de la separación de las zonas ecuatorial y septentrional.
Esqueleto de Giraffatitan, un saurópodo del Jurásico superior pariente cercano del Brachiosaurus.

El desarrollo de reinos diferenciados obedecía a la existencia de barreras marinas entre el norte y el sur, y a la presencia de un mayor gradiente de temperaturas desde los polos hasta el ecuador. Los gradientes térmicos no eran tan pronunciados como lo son actualmente, no existen pruebas de hielo polar durante el Jurásico, y la flora alejada del ecuador correspondía a plantas de zonas templadas. Los paisajes jurásicos estaban dominados por Cycadophyta, y por sus parientes las Bennettitales (cicadeoideas), con aspecto de piñas gigantes cubiertas, en la estación propicia, por llamativas «flores» que no eran auténticas flores. Los bosques de ginkgos, y especialmente de coníferas, daban al paisaje cierto toque de modernidad, pero las plantas con verdaderas flores, los árboles de madera dura y especialmente las hierbas, todavía estaban ausentes.
Fauna
Vertebrados terrestres

Las primeras ranas aparecen en el Jurásico. Los cocodrilos se encontraban ya plenamente establecidos. Grandes reptiles arcosaurios permanecían como dominadores del ecosistema terrestre. Desafortunadamente, el registro fósil conocido del Jurásico inferior es demasiado pobre para permitirnos detalles sobre la diversificación de los dinosaurios, aunque los restos fósiles de enormes dinosaurios que aparecen en rocas jurásicas indican que evolucionaron rápidamente. En este periodo los saurópodos aumentaron significativamente su tamaño, como Diplodocus y Brachiosaurus. Los depredadores también crecieron adaptándose a nuevas metodologías de caza. Algunos como el Allosaurus dominaron las tierras del Jurásico. Además surgieron otros grandes dinosaurios fitófagos como Stegosaurus, con placas óseas en la espalda y defensas espinosas en la cola.

    Brachiosaurus (Sauropoda)

    Allosaurus (Carnosauria)

    Stegosaurus (Ornithischia)

    Chaoyangsaurus (Ornithischia)

    Scutellosaurus (Ornithischia)

Vertebrados aéreos

En este periodo aparecen las primeras aves de pequeño tamaño. Pero lo que en realidad dominaba los cielos en estos tiempos fue los grandes pterosaurios. Esta familia de reptiles, que no eran dinosaurios, se alimentaba de los peces en los grandes mares con sus largos picos que poseían dientes puntiagudos. Dieron a un nuevo grupo, los pterodactiloideos, entre los que destaca Pterodactylus, que fue hallado en África oriental, Inglaterra y Francia. La cola de estos era muy corta, su cabeza era mayor y su cuello más largo que los pterosaurios triásicos.

    Archaeopteryx (Aves)

    Pterodactylus (Pterosauria)

Vertebrados acuáticos

Durante el Jurásico las más evolucionadas formas de vida marina eran los peces y los reptiles. Los ictiosaurios sobreviven al cambio de periodo. Compartían los mares con los primeros cocodrilos acuáticos, los cuales tenían aletas en vez de patas, y con los teleósteos, predecesores de la mayoría de los peces actuales. Los plesiosaurios son otro grupo destacado en este periodo que no pudo llegar al siguiente, al igual que los cocodrilos con aletas.

    Ophthalmosaurus (Ichthyosauria)

    Plesiosaurus (Plesiosauria)

    Leedsichthys (Actinopterygii)

Véase también

    Era Mesozoica
    Geología histórica

Notas y referencias

Los colores corresponden a los códigos RGB aprobados por la Comisión Internacional de Estratigrafía. Disponible en el sitio de la International Commision on Stratigraphy, en «Standard Color Codes for the Geological Time Scale».

    «International Commission on Stratigraphy: International Chronostratigraphic Chart v2015/01» (en inglés). Enero de 2015. Consultado el 12 de octubre de 2015.

Bibliografía

    Behrensmeyer, Anna K., Damuth, J.D., DiMichele, W.A., Potts, R., Sues, H.D. y Wing, S.L. (eds.) (1992), Terrestrial Ecosystems through Time: the Evolutionary Paleoecology of Terrestrial Plants and Animals, University of Chicago Press, Chicago y Londres, ISBN 0-226-04154-9, ISBN 0-226-04155-7
    Haines, Tim (2000) Walking with Dinosaurs: A Natural History, Nueva York: Dorling Kindersley Publishing, Inc., p. 65. ISBN 0-563-38449-2
    Monroe, James S., y Reed Wicander. (1997) The Changing Earth: Exploring Geology and Evolution, 2ª ed. Belmont: West Publishing Company, 1997. ISBN 0-314-09577-2

Enlaces externos

    Jurásico La deriva continental (Proyecto Celestia)
    Museo del Jurásico de Asturias
    Ruta del Jurásico
    http://www.geo-lieven.com/erdzeitalter/jura/jura.htm
    http://www.duiops.net/dinos/jurasico.html
    http://club.telepolis.com/argosub/tiburones/jurasico.htm
    http://www.educarm.es/paleontologia/vitjurasico.htm

Categorías:

 * JurásicoPeriodos geológicos del Mesozoico

Menú de navegación

 * No has iniciado sesión

Imprimir/exportar

 * Crear un libro
 * Descargar como PDF
 * Versión para imprimir