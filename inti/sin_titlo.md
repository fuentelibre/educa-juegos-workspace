## flores:

####  Dracaena draco

[![Dracaena_draco](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Dracaena_draco_%28Puntagorda%29_12_ies.jpg/220px-Dracaena_draco_%28Puntagorda%29_12_ies.jpg) ![a](https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Drago_ram.jpg/120px-Drago_ram.jpg) ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/Berthelot_drago_franchy.jpg/220px-Berthelot_drago_franchy.jpg) ![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/D.draco-3.jpg/90px-D.draco-3.jpg) ![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/D.draco-1.jpg/120px-D.draco-1.jpg) ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Dracaena_draco_fruits.JPG/120px-Dracaena_draco_fruits.JPG) ![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/D.draco-6.jpg/120px-D.draco-6.jpg) ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/D.draco-8.jpg/120px-D.draco-8.jpg)<br>Dracaena draco](https://es.wikipedia.org/wiki/Dracaena_draco).

## colibris:

### género [Amazilia](https://es.wikipedia.org/wiki/Amazilia):

#### amazilia bronceada de cola azul

[![Amazilia tobaci](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Amazilia_tobaci_-_perched.JPG/270px-Amazilia_tobaci_-_perched.JPG) Amazilia tobaci](https://es.wikipedia.org/wiki/Amazilia_tobaci)

#### amazilia listada


[![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Amazilia_fimbriata.jpg/220px-Amazilia_fimbriata.jpg) Amazilia fimbriata](https://es.wikipedia.org/wiki/Amazilia_fimbriata)

#### colibrí común de Lima:


[![Amazilia amazilia](https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Amazilia_amazilia-4.jpg/250px-Amazilia_amazilia-4.jpg) Amazilia amazilia](https://es.wikipedia.org/wiki/Amazilia_amazilia).


#### Colibrí esmeralda de Honduras

[![bh](https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Amazilia_luciae.JPG/275px-Amazilia_luciae.JPG) Amazilia luciae](https://es.wikipedia.org/wiki/Amazilia_luciae).



#### diamante de frente azul
[![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Indigo-capped_Hummingbird_%28Amazilia_cyanifrons%29_%288079781711%29.jpg/250px-Indigo-capped_Hummingbird_%28Amazilia_cyanifrons%29_%288079781711%29.jpg) Amazilia cyanifrons](https://es.wikipedia.org/wiki/Amazilia_cyanifrons)

### género Ensifera:

##### Colibrí picoespada
[![mn](https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Sword-billed_Hummingbird.jpg/250px-Sword-billed_Hummingbird.jpg) ![fgg](https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Ensifera_ensifera.jpg/120px-Ensifera_ensifera.jpg) Ensifera ensifera](https://es.wikipedia.org/wiki/Ensifera_ensifera).

### género Aglaiocercus:
https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Bufo_periglenes1.jpg/800px-Bufo_periglenes1.jpg
##### Cometa colivioleta o silfo celeste

![fg](https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Violet-tailed_Sylph_%28Aglaiocercus_coelestis%29.jpg/250px-Violet-tailed_Sylph_%28Aglaiocercus_coelestis%29.jpg)   .