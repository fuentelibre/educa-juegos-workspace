* [Amazona amazonica](https://es.wikipedia.org/wiki/Amazona_amazonica) - [Loro de ala naranja.](https://es.wikipedia.org/wiki/Amazona_amazonica)
  
  * [Amazona farinosa](https://es.wikipedia.org/wiki/Amazona_farinosa) - [Loro harinoso](https://es.wikipedia.org/wiki/Amazona_farinosa).
  
  * [Amazona festiva](https://es.wikipedia.org/wiki/Amazona_festiva) - [Loro de lomo rojo](https://es.wikipedia.org/wiki/Amazona_festiva).

  * [Amazona mercenaria](https://es.wikipedia.org/wiki/Amazona_mercenaria) - [Loro de nuca escamosa](https://es.wikipedia.org/wiki/Amazona_mercenaria).

  * [Amazona ochrocephala](https://es.wikipedia.org/wiki/Amazona_ochrocephala) - [Loro de corona amarilla](https://es.wikipedia.org/wiki/Amazona_ochrocephala).

  * [Ara ararauna](https://es.wikipedia.org/wiki/Ara_ararauna) - [Guacamayo azul y amarillo](https://es.wikipedia.org/wiki/Ara_ararauna).

  * [Ara chloropterus](https://es.wikipedia.org/wiki/Ara_chloropterus) - [Guacamayo roja y verde](https://es.wikipedia.org/wiki/Ara_chloropterus).

  * [Ara macao](https://es.wikipedia.org/wiki/Ara_macao) - [Guacamayo escarlata](https://es.wikipedia.org/wiki/Ara_macao).

  * [Ara militaris](https://es.wikipedia.org/wiki/Ara_militaris) - [Guacamayo militar](https://es.wikipedia.org/wiki/Ara_militaris).

  * [Ara severus](https://es.wikipedia.org/wiki/Ara_severus) - [Guacamayo de frente castaña](https://es.wikipedia.org/wiki/Ara_severus).

  * [Aratinga aurea](https://es.wikipedia.org/wiki/Eupsittula_aurea) - [Cotorra de frente dorada](https://es.wikipedia.org/wiki/Eupsittula_aurea).

  * [Aratinga erythrogenys](https://es.wikipedia.org/wiki/Psittacara_erythrogenys) - [Cotorra de cabeza roja](https://es.wikipedia.org/wiki/Psittacara_erythrogenys).

  * [Aratinga leucophthalmus](https://es.wikipedia.org/wiki/Psittacara_leucophthalmus) - [Cotorra de ojo blanco](https://es.wikipedia.org/wiki/Psittacara_leucophthalmus).

  * [Aratinga mitrata](https://es.wikipedia.org/wiki/Psittacara_mitratus) - [Cotorra mitrada](https://es.wikipedia.org/wiki/Psittacara_mitratus).

  * [Aratinga wagleri](https://es.wikipedia.org/wiki/Psittacara_wagleri) - [Cotorra de frente escarlata](https://es.wikipedia.org/wiki/Psittacara_wagleri).

  * [Aratinga weddellii](https://es.wikipedia.org/wiki/Aratinga_weddellii) - [Cotorra de cabeza oscura](https://es.wikipedia.org/wiki/Aratinga_weddellii).

  * [Bolborhynchus lineola](https://es.wikipedia.org/wiki/Bolborhynchus_lineola) - [Perico barrado](https://es.wikipedia.org/wiki/Bolborhynchus_lineola).

  * [Bolborhynchus orbygnesius](https://es.wikipedia.org/wiki/Bolborhynchus_orbygnesius) - [Perico andino](https://es.wikipedia.org/wiki/Bolborhynchus_orbygnesius).

  * [Brotogeris cyanoptera](https://es.wikipedia.org/wiki/Brotogeris_cyanoptera) - [Perico de ala cobalto](https://es.wikipedia.org/wiki/Brotogeris_cyanoptera).

  * [Brotogeris pyrrhoptera](https://es.wikipedia.org/wiki/Brotogeris_pyrrhoptera) - [Perico de mejilla gris](https://es.wikipedia.org/wiki/Brotogeris_pyrrhoptera).

  * [Brotogeris sanctithomae](https://es.wikipedia.org/wiki/Brotogeris_sanctithomae) - [Perico tui](https://es.wikipedia.org/wiki/Brotogeris_sanctithomae).

  * Brotogeris versicolurus - Perico de ala amarilla.

  * Deroptyus accipitrinus - Loro de abanico.

  * Diopsittaca nobilis - Guacamayo enano.

  * Forpus coelestis - Periquito esmeralda.

  * Forpus sclateri - Periquito de pico oscuro.

  * Forpus xanthops - Periquito de cara amarilla. 

  * Forpus xanthopterygius - Periquito de ala azul.

  * Graydidascalus brachyurus - Loro de cola corta.

  * Hapalopsittaca amazonina - Cotorra montañera.

  * Hapalopsittaca melanotis - Loro de ala negra.

  * Hapalopsittaca pyrrhops - Loro de cara roja.

  * Leptosittaca branickii - Perico de mejilla dorada.

  * Nannopsittaca dachilleae - Periquito amazónico.

  * Orthopsittaca manilata - Guacamayo de vientre rojo.

  * Pionites leucogaster - Loro de vientre blanco.

  * Pionites melanocephalus - Loro de cabeza negra.

  * Pionus chalcopterus - Loro de ala bronceada.

  * Pionus menstruus - Loro de cabeza azúl.

  * Pionus tumultuosus - Loro tumultuoso.

  * Pionus sordidus - Loro de pico rojo.

  * Primolius couloni - Guacamayo de cabeza azúl.

  * Psilopsiagon aurifrons - Perico cordillerano.

  * Pyrilia barrabandi - Loro de mejilla naranja.

  * Pyrrhura lucianii - Perico de Bonaparte.

  * Pyrrhura melanura - Perico de cola marrón.

  * Pyrrhura molinae - Cotorrilla mejilla verde.

  * Pyrrhura peruviana - conuro de pecho ondulado.

  * Pyrrhura roseifrons - Perico de frente rosada.

  * Pyrrhura rupicola - Perico de gorro negro.

  * Touit huetii - Periquito de ala roja.

  * Touit purpurata - Periquito de lomo zafiro.

  * Touit sticopterus - Periquito de ala punteada.