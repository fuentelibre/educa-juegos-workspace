# Blenda

La **blenda** o **esfarelita** es un mineral compuesto por sulfuro de zinc (ZnS). Su nombre deriva del alemán blende, "lucir, brillar, cegar". El nombre de esfalerita proviene del griego sphaleros, engañoso (su aspecto se confunde con el de la galena).
        
        Blenda

![v](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Blendeperou2.jpg/245px-Blendeperou2.jpg)

##### Aspecto

En masas compactas o fibrosas, son frecuentes las maclas. El sulfuro de cinc es incoloro, pero la blenda siempre tiene sulfuro de hierro (II) (FeS), lo que la oscurece. Cuando el porcentaje de hierro es bajo, se le llama blenda acaramelada, mientras que si su contenido de hierro es alto, se le llama marmatita.

##### Utilización

Es la principal mena de zinc, metal que se utiliza para galvanizar el hierro impidiendo su oxidación y en aleación con cobre da el latón. El óxido de cinc (blanco de zinc) se emplea en la fabricación de pinturas, su cloruro en la conservación de la madera y su sulfato en tintorería y farmacología. La blenda es una de las principales menas de cadmio, indio, galio y germanio, que aparecen en pequeñas proporciones sustituyendo al zinc.

##### Yacimientos

Uno de los principales yacimientos del mundo es el de Áliva, en Cantabria, de donde proceden las blendas de mejor calidad del mundo por su transparencia, variedad de colores y pureza.[cita requerida] Se puede encontrar una antigua mina de este mineral en el Cerro del Toro, en Motril (Granada). Otra mina abandonada se halla en la isla de Colom, Menorca.

![f](https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/Sphalerite_taillee.jpg/220px-Sphalerite_taillee.jpg)