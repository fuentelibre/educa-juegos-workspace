# Update Plan

Objective: 
- To update apps to run under Ruby 2.3+
- To reach Debian 9 accross all servers

Doing so with the least possible disruption of service.

## Solution

- Prerequisite: Setting up Passenger in Docker container (to match current production)
- Start plots2+ruby2.3+ instance at new separate server at Rackspace:
    - reusing Solr at pad
    - separate database instance, 2-way-synchronized with production
- Point production Nginx to proxy to new instance
    - Shall we point DNS to new instance too? 
      This would avoid short downtime when updating nginx / rebooting.
- Update production / deploy containers
    - Prune base install of any not-required packages beforehand
- Update nginx config to point to new setup