## Publication workflow

## 1. start at a PR: say "we should publish this, @publiclab/sysadmin" referring to the latest master by ananyo or jeff, with contributor in the thread
## 2. somebody (@publiclab/infrastructure) takes responsibility and initiates a merge of master into stable (possibly with a comment like "@publiclab/build start" from a known user), Jenkins build starts

Simplified version:
- git fetch
- git checkout origin/master
- git push origin HEAD:stable

** I don't know how to use a URL remote for the checkout step.
    by default git names the clone origin as `origin` remote. ~~icarito

Jeff version:
- git checkout master (on local machine)
- git pull https://github.com/publiclab/plots2 master
- git checkout stable
- git merge master
- (confirm it's fast-forward, no merge)
- git push https://github.com/publiclab/plots2 stable
- git checkout master (so we don't 'hang out' on stable)

## 3. on confirmation that Jenkins built successfully, publish to production server (in container)

- `mosh publiclab.org`
- open/recover screen session with `screen -r`
- 2 screen tabs open, one as user 'plots' and one as 'warren' (sudoer)
- as 'plots' - `git pull https://github.com/publiclab/plots2 stable`
- confirm it's ff only, no merges
- if it merges, i roll back:
    - make quick temp commit message when prompted
    - look back with `git log` and copy the hash of the last known working commit
    - `git reset --hard COMMIT_HASH` (scary/bad)
- look for changes to bower.json and if they exist, `bower install`
- `bower update` (updates bower packages to latest in specified range)
- (once we complete switch from Bower to NPM) look for changes to package.json and if they exist, `npm install`
- `npm update` (updates npm packages to latest in specified range)
- look for changes to /assets/, (css, js) and if there are any, `rake assets:precompile`
- starting to get quite long, we could look into precompiling assets and git tracking them... look at best practices online
- if any changes to /db/, run `rake db:migrate` and hold breath
- switch to sudoer username 'warren'
- run `sudo service nginx restart`
- confirm on a few pages of publiclab.org

4. report back to members of the PR comment thread: "This is now live on PublicLab.org! Thanks!"

