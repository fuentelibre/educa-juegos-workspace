# Public Lab Infrastructure

## Operating Procedures

We run a number of applications of varying load and uptime requirements.

This document is intended to be a guide for managing services including how to start and configure the required components.

## Generic procedures

These are generally how we operate Rails apps, once they have been installed.
To facilitate startup we've written a very simple 'start_containers' script that just uses the proper environment variables including setting Rails environment, Solr host and any needed flags, as well as the docker-compose-_something_.yml file that is used to define which containers are needed in this service.

We've not made equivalent `stop` and `restart` scripts in order to be familiar with the underlying process. It is suggested to move these details into `Makefile` in the future.

```
# This is the production application location
cd /srv/plots/production

# start containers
./start_containers

# destroy containers (use appropriate yml file for each case)
docker-compose -f docker-compose-production.yml down

# stop specific container
SOLR_URL=http://solr:8983/solr/default/ RAILS_ENV=production docker-compose -f docker-compose-unstable.yml stop web

# Often to restart a rails container you will have to remove (it's owned by root)
sudo rm tmp/pids/server.pid

# start specific container
SOLR_URL=http://solr:8983/solr/default/ RAILS_ENV=production docker-compose -f docker-compose-unstable.yml start web

# (To restart, do all three steps above)

# check logs
docker-compose -f docker-compose-production.yml logs -f --tail=500

# reindex solr container (currently running on pad)
RAILS_ENV=production docker-compose -f docker-compose-solr.yml run web rake sunspot:solr:reindex DISABLE_SOLR_CHECK=1

# passenger rails app is setup to start/stop with nginx
# start
service nginx start
# stop
service nginx stop
```

## Status of container deployment

### Plots2 (3 services)

 - plots2 running on rs-plots2 <font color='orange'>host</font>
 - mysql running on rs-plots2 <font color='green'>container</font>
 - solr running on pad <font color='green'>container</font>
 
### MapKnitter (2 services)

 - plots2 running on rs-tycho2 <font color='orange'>host</font>
 - mysql running on rs-tycho2 <font color='orange'>host</font>

 - priv instance running on tycho <font color='green'>2 containers</font>
