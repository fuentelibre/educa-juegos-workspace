Estimados amigos de las telecomunicaciones

En noviembre de 2006, la Conferencia de Plenipotenciarios de la Unión Internacional de Telecomunicaciones (UIT) reunida en Antalya (Turquía) decidió celebrar ambos eventos, Día Mundial de las Telecomunicaciones y Día Mundial de la Sociedad de la Información, el 17 de mayo.[1] 

La celebración de hoy día debe:

00 Contribuir a que se conozcan mejor las **posibilidades que pueden brindar Internet y otras tecnologías de la información y las comunicaciones** a las sociedades y economías. 

01 Contribuir a preparar planes de acción y políticas para reducir la brecha digital existente en el **acceso a las tecnologías de la información y las comunicaciones**.


En ese sentido, la Asamblea insta a construir una sociedad de la información centrada en las personas, integradora y orientada al desarrollo.

1. APOYAR y escalar la I + D para comprender y explorar las oportunidades donde la Inteligencia Artificial
tiene el mayor impacto posible, particularmente en su impacto en los Objetivos de Desarrollo Sostenible
2. IDENTIFICAR cómo la IA contribuirá al logro de los ODS mediante, por ejemplo, una mayor eficiencia de
sistemas o metodologías para apoyar la toma de decisiones basada en evidencia.
3. APOYAR los esfuerzos internacionales en actividades de colaboración en todos los sectores y naciones para fomentar
innovación usando la Inteligencia Artificial.
4. FOMENTAR el intercambio de experiencias sobre el uso de la Inteligencia Artificial en todos los sectores y
fronteras.
5. CONTEXTUALICE el uso de la Inteligencia Artificial a sus propias necesidades regionales y nacionales.
6. DESARROLLAR políticas y estrategias nacionales que promuevan el uso de la Inteligencia Artificial para bien y
para todos.








[1] Tomado de Wikipedia, "Día Mundial de las Telecomunicaciones y de la Sociedad de la Información" el 17 de Mayo de 2018 

[2] Tomado de 


