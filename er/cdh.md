En el caso del Karate-Dō, la ética deriva de las filosofías del confucianismo y del budismo zen, aplicados al Karate-Dō. Estos principios fundamentales están basados en el código de los guerreros medievales japoneses o samurai, llamado bushidō. En resumen, estos se podrían sintetizar como los siguientes:
* La Cortesía: el respeto y las buenas maneras del comportamiento
