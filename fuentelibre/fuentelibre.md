# Descubre

*Afirmado por icarito el Vie, 11/28/2008 - 02:00.*

FuenteLibre es una comunidad de aprendizaje con fines sociales. Es una organización abierta, una cooperativa entre individuos, con el objetivo de expandir los espacios disponibles para el aprendizaje comunitario en red.

El primer paso para participar de nuestra comunidad es informarse sobre nuestra misión y nuestros objetivos.

FuenteLibre existe en el espacio común entre la comunidad de tecnología libre y la educación, sobretodo rural. Hace tan solo unos pocos años, este espacio no existía.

Queremos armonizar este primer contacto, hacer un puente entre ambas culturas, para mitigar el choque y maximizar el retorno social.