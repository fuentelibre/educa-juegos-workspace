# Objetivos

## Objetivos Educacionales

* Proporcionar acceso a herramientas y obras educativas libres
* Explorar y compartir experiencias y prácticas pedagógicas
* Proporcionar un espacio de diálogo y apoyo para tecnología educativa
* Proporcionar mecanismos de evaluación, difusión y retroalimentación

## Objetivos Comunitarios

* Traducir y adaptar las tecnologías para ser apropiadas por la cultura local
* Explorar un modelo replicable, escalable y abierto de organización
* Proveer servicios de desarrollo, integración, capacitación y soporte

## Objetivos Tecnológicos

* Extender Azúcar (Sugar) y su gama de actividades
* Extender tecnología de redes sociales libres
