# Misión

***"Fortalecer las redes de comunidades de aprendizaje"***

Llamamos ampliamente “comunidad de aprendizaje” a un conjunto de individuos con valores similares y una meta en común, que colaboran para construir conocimiento y soluciones relevantes a su contexto social.

Escuelas, asociaciones gremiales, grupos de estudio y otros, pueden ser ejemplos de comunidades de aprendizaje, así como también las existen virtuales, por ejemplo la comunidad de software libre o la comunidad de Wikipedia.

El proceso de aprendizaje comunitario supone la libertad de “explorar, expresar, depurar y criticar” ideas en un espacio común.

Fortalecemos este espacio de conversación facilitando los canales de comunicación y también combatiendo las barreras técnicas, legales y culturales para que ella ocurra.