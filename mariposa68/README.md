# Mariposa 68 - Butterfly 68

**Authors**: Colectivo Intamalase Collective

**Description**: _La obra es una ofrenda al río pidiéndole con humildad **la cura para el sútil sagrado femenino**, compuesto por los brillos de la vida que es albergada a su alrededor._ The work is an offering to the river, humbly asking for healing of the subtle sacred feminity, composed by the shines of the life sheltered around it.

This work is part of a series of works resulting from our family programming and art workshop, with our young children - using the following Software: Jappy IDE (educa.juegos), Gimp, Inkscape, Audacity.

### Butterfly 68 / Mariposa 68: _Diaethria Clymena_

Río Tambopata, Madre de Dios, Amazonian rainforest

Photos and audio taken at Tambopata River, April 2018

* Creation: Madre Naturaleza / Mother Nature
* Photography: Laura Vargas & Sebastian Silva
* Audio: Narowé Silva Vargas & Laura Vargas
* Producer: Laura Vargas & Inti Silva Vargas
* Script: Tao Silva Vargas & Laura Vargas
* Programming: Sebastian Silva & Narowé Silva Vargas

License: [**AGPL v3**](LICENSE)
