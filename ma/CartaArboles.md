# Mensaje de los árboles a la humanidad
![](https://educa.juegos/dav/ma//1.png)
Queridos hermanos humanos, por favor, no permitan que nos corten!

Nosotros los árboles y las plantas declaramos que co-existimos en el mismo espacio-tiempo que ustedes. Aunque nuestro movimiento es limitado,tenemos plena conciencia derivada de nuestra presencia e inteligencia. Estamos vivos!

Por favor, no permitan que nos corten!

Nosotros los árboles y las plantas sabemos comunicarnos y les traemos este mensaje hoy, aquí, ahora;

Amamos y damos soporte de vida a todas las criaturas. 

Damos abrigo y resguardo, agua, aire, ricos frutos  y hermosos paisajes. 

Nuestras raíces profundas toman agua del subsuelo y la elevan por nuestras ramas hasta nuestras hojas donde emitimos Oxígeno a la atmósfera como parte de nuestro ciclo de vida.

Eso hace que la atmósfera sea respirable para las demás criaturas, entre ellas ustedes hermanos seres humanos.

Por favor no permitan que nos corten!

Dentro del ecosistema, somos necesarios para mantener el equilibrio bio.electro.químico de la atmósfera. Nuestro objetivo es continuar contribuyedo al ciclo de la lluvia;
 
Dependemos los unos de los otros!

Por favor, no permitan que nos corten!


# Los árboles y las plantas del planeta Tierra
# Seres vivos 75% agua (igual que los seres humanos) 

![](https://educa.juegos/dav/ma//40.png)
