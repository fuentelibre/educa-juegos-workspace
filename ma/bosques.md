Bosques Libres - Metodología y proyecto piloto para aumentar la salud y la cohesión de las comunidades

La idea es habilitar una trocha recorrible a pie en un bosque maduro [1], monitoreada, alumbrada y sostenida comunitariamente mendiante uso exclusivo de *tecnología libre* [2] con el objetivo de exponer a los comuneros al uso de sistemas (digitales y análogos) y a la vez disponer para ellos el acceso libre a "los baños de los boques" en Japonés "Shinrin-Yoku", una práctica simple para *mejorar la salud* de los ciudadanos.


[1] Tecnología Libre entendida como aquella tecnología que ha sido licenciada de forma apropiada y por lo tanto es de libre acceso, modificación y distribución.

[2] Bosque maduro teniendo en cuenta una aproximación
de la distribución de edades de acuerdo a la distribución de diámetros por especie. 
