
|Término|Explicación|
|-------|-----------|
|IAEP|_It's An Education Project_. Lista de correo principal del proyecto Sugar Labs.|
|OLPC|_One Laptop Per Child_. Puede referir a OLPC Foundation, OLPC Association u OLPC Inc.|
|Sugar|Entorno de Aprendizaje cuyos pilares son la Simplicidad, la Colaboración y la Reflexión.|
|Sugar Labs|Proyecto bajo el alero de la Software Freedom Conservancy|
