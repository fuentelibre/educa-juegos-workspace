# Resumen

Este trabajo presenta la experiencia de dos voluntarios apasionados por el conocimiento y especialmente la definición de procesos para asegurar la calidad del software libre para niños. 

Sebastian y Laura se conocieron el Día del Software Libre (SFD) 19 de Septiembre de 2009. Aquel día Sebastian, presentaba su conferencia “Pedagogía del Oprimido – Software de la Liberación” mientras que Laura, escuchaba atentamente aquel mensaje liberador; era la llamada a la ética y a la acción. Sebastian invitaba a todos los asistentes a apropiarse del software libre llamado "Sugar" que sería distribuido a millones de niños en los años siguientes en los países en vías de desarrollo. Fué una llamada irresistible. Y así inició esta historia, esta historia que nos ha traído hasta "Sugar 0.111-libre" y que sobre todas las cosas es una historia de amor.  

Hoy, mientras educamos a nuestros hijos con la ayuda de Software Libre apropiado, continuamos celebrando la libertad del Software haciendo un balance de nuestros aprendizajes como equipo. Múltiples dinámicas han sido exploradas desde aquel día de Libertad, con el objetivo de identificar buenas prácticas que faciliten la comunicación y la apropiación de las tecnologías libres entre los usuarios del sistema educativo. 

En 2008 Sebastian organizó los primeros grupos de voluntarios que se propusieron apoyar el despliegue de estas computadoras portátiles en Perú, reuniendo inicialmente una lista de 120 personas. El choque fué estrepitoso, pues los voluntarios no encontraron espacio en el marco de trabajo planteado entre el fabricante de las máquinas (OLPC) y el Ministerio de Educación de Perú. Las puertas a la colaboración estaban cerradas. La mayor preocupación ante esta situación, era que la imagen de sistema operativo instalada en terreno era una imagen de pruebas y estaba llena de errores y frustación para los aprendices y maestros. 

Así entonces con el apoyo de Laura, a partir de 2009 cambiamos de estrategia. Con los escasos recursos que teniamos y con el apoyo incondicional de Escuelab Lima y demás activistas que nos han apoyado en el camino, nos trazamos un objetivo claro: construiríamos y entregariamos al Ministerio de Educación de Peru, una imagen de sistema operativo actualizada que mejorara la experiencia de uso del software.

Creamos un grupo de investigación y desarrollo, inventamos una marca e iniciamos nuestro viaje hacia aquella imagen. Rápidamente las lenguas nativas tomaron prioridad y así formulamos una estrategia para lograrlas; nuestro primer evento fué una Maratón de Traducción; "Miski Pachamama 2010". A orillas del lago Titicaca y con el apoyo de la sociedad civil movilizada ante el llamdo del Software Libre, se lograron las primeras traducciones al Quechua y al Aimara de "Sugar".

Pero las traducciones no eran suficiente. Se requería movilizar hackers de todo el mundo para responder ante el reto técnico de conformar una imagen de sistema operativo actualizada y optimizada para la experiencia local peruana. Y asi llegaron también los hackers a Perú a Sugar Camp 2011.

Nos presentaríamos al Ministerio de Educación con un producto listo, como un proveedor, que resolviera una necesidad concreta y que estuviera alineado con nuestro mensaje de apropiación. Hicimos una campaña y convocamos a la comunidad quechua-hablante y aymara-hablante para traducir a estas lenguas el entorno educativo. Organizamos eventos en Puno y Lima, logrando traducir de este modo más del 60% de Sugar.

Esta sería nuestra oportunidad de tener un impacto positivo y transmitir nuestro mensaje. Convocamos a la comunidad global para Sugar Camp Lima 2011 y vino gente de varios países del mundo y también desde Puno, para colaborar en este proceso. En 2010 - 2011 Sebastian fue elegido para la junta de supervisión de Sugar Labs (SLOBS).

Para ese entonces el Ministerio de Educación despertó a la necesidad de estas lenguas en la interfase de usuario y, viendo que había un trabajo ya avanzado, nos contactó para que los asesoráramos en la metodología para llevar a cabo esta traducción e incluir estas traducciones en imágenes instalables de sistema operativo, para su distribución a nivel nacional.

Desarrollaríamos una imagen de sistema operativo actualizada que incluyera un mecanismo de comunicación descentralizado para permitir que educadores, padres, niños, desarrolladores y voluntarios tuvieran un diálogo directo.

Gracias a algunas donaciones y pequeñas contrataciones con el Ministerio de Educación, Laura y Sebastian lograron trabajar en colaboración con Aleksey Lim (Rusia) quien estando en Perú en 2012 diseñó la arquitectura e implementó Sugar Network, la primera red descentralizada y libre dedicada a Recursos Educativos Abiertos en Latinoamérica (o el mundo?).

En 2014 el Ministerio de Educación empezó el despliegue de "Hexoquinasa" - que ellos llamaron "Sugar 9" - nuestra imagen de sistema operativo actualizada, que incluía Quechua y Aymara, así como el acceso directo a la Red Azúcar - Sugar Network desde el mismo escritorio.

Por estos tiempos los niveles de actividad en las listas de correo de Sugar iban en declive así el ritmo de desarrollo. Sugar había llegado a planicie y no habiendo sido apropiado por ninguna comunidad excepto sus propios creadores, cayó en un estancamiento en cuanto a su evolución.

No solamente no se hicieron mejoras, sino que se descuidó el código hasta el punto que muchas de los paquetes dependen de bibliotecas obsoletas, lo cual amenaza con sacar a Sugar de Debian y otras distribuciones.

Incluso la junta de supervisión de Sugar, por falta de interesados y comité de elecciones, declaró desierta la elección en 2014 y lo hubieran hecho denuevo en 2015 sino es porque Sebastian (impulsado por Laura) pide que se le nombre comité de elecciones y se lleve a cabo una convocatoria nueva de candidatos.

En 2015 nos supimos que algunos miembros fundadores de la junta habían obtenido un _"grant"_ de ciento veinte mil dólares para contribuir al desarrollo de Sugar en lenguas nativas. 

Sugar Labs solo recibió ochenta mil, pues no se cumplió el requisito de informar al donante respecto del uso del dinero. Este dinero no había sido gastado, excepto por algunos viajes de estos fundadores del proyecto a eventos y contrataciones pequeñas a traductores, sin consulta con la comunidad.

Viendo una oportunidad de sostener otro intento de participar en la mejora del sistema, Laura presionó a la junta para que apoyaran procesos de traducción externos y de este modo se hicieron las traducciones de Igbo y Yoruba (Nigeria).

A finales de 2016, otra vez la junta pretendía extender su mandato por falta de comité de elecciones, y Laura se ofreció a dirigirlas junto a Ignacio (Uruguay) y Samson (Nigeria). Había tres puestos disponibles y los tres fueron también los únicos candidatos, por lo que su elección fue automática y continúan en la junta hasta hoy.

Sin embargo hasta la fecha la junta se ha negado a aprobar fondos para el desarrollo mínimo de Sugar Network (mantenimiento y prevención de _spam_), mucho menos contempla la posibilidad de dedicar fondos para contratar personas en procesos abiertos para mejorar Sugar como tal.

Con todo, Sugar es software libre, y tenemos la esperanza de que con perseverancia lograremos desembarazar a Sugar del bagaje que trae y lo adaptaremos a nuestras necesidades locales en conjunto con las comunidades de Software Libre locales que son cada vez más fuertes.

Si para romper con las prácticas y la cultura anterior, será necesario abrir el desarrollo a un proyecto aparte, esto está aún por verse.
