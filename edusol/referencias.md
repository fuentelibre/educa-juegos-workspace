"Technology and Child Development: Evidence from the One Laptop Per Child Program"

https://papers.ssrn.com/sol3/Delivery.cfm/SSRN_ID2032444_code170891.pdf?abstractid=2032444&mirid=1

https://wiki.sugarlabs.org/go/Trademark/Guidelines

https://thenounproject.com/term/flying-heart/227517/

network.sugarlabs.org/stats-viewer/



One of the challenges that free software projects face is the impact of governance on their community members: while FLOSS licenses assure access to source code, that doesn't guarantee a successful project. A governance model can help ensure that the project is run in a professional, disciplined, and equitable manner. Good governance lets the community engage in discourse and provides a transparent mechanism for arbitration in the hopefully rare circumstances in which it is necessary.

Some attributes that are necessary for good governance include: meritocracy, transparency of process, open access to anyone who has demonstrated the skills to contribute, and a means to ensure a balance of control so that no one special interest wrests control of either the discourse or the decision-making. 