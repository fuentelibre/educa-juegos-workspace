# _Intro_
## Resumen

Este trabajo presenta la experiencia de un equipo motivado por el ideal del conocimiento libre y nuestro objetivo al compartirlo es dar cuenta de soluciones intentadas y con ello informar y facilitar nuevas iniciativas.

Aunque creemos que una solución debe ser transversal a la plataforma, género y edad de los usuarios, también creemos que la sociedad debe prestar especial atención a la calidad del software cuyo objetivo es atender a la población comprendida en las etapas escolares tempranas [Referencia: El software SUGAR es distribuido en Latam desde 2007 y está orientado a niños y niñas entre 4 y 10 años]. 


## Colaboramos en el despliegue, desarrollo y apropiación de la tecnología educativa "Sugar".

 * 01 Primero formamos un cuerpo de voluntarios.
 * 02 Luego convocamos _maratones de traducción a lenguas nativas_ Quechua y Aymara.
 * 03 Conformamos el equipo técnico que actualizó el sistema operativo en las laptops educativas de Perú, bajo encargo del Ministerio de Educación de Perú.
 * 04 Publicamos cinco videojuegos didácticos, herramientas y materiales, desarrollados en un taller junto a una docena de niños de 10-12 años, en 14 sesiones.
 * 05 Tres videojuegos para practicar vocabulario en Inglés con algoritmo de repaso espaciado (que estimula la memoria)
 * 06 Hemos implementado mejoras en el entorno Sugar, tales como facilitar la ejecución de Actividades de Sugar en entornos de GNU/Linux convencionales.
 * 07 Colaboramos voluntariamente en diversas tareas de servicio dentro de Sugar Labs tales como gestionar las listas de correo, integrar el equipo de infrastructura de servidores, el comité de membresía y elecciones, y en dos oportunidades integramos la junta de supervisión.
 

_¿CON QUIÉN LO HIZO?_

Con numerosos otros voluntarios cuyas motivaciones y estrategias variaban ampliamente. Se hizo un llamado amplio en busca de tener un impacto positivo en los aspectos logísticos, técnicos y sociales inherentes a una implementación tecnológica.

Hubo 120 inscritos peruanos en el cuerpo de voluntarios bautizado **FuenteLibre.Org** en 2007 y 2008. Recibimos un apoyo de 700 dólares directamente de Richard Stallman, quien había recaudado esta suma en Perú subastando libros y otros objetos. Con estos fondos llevamos a cabo una campaña que llevó a los fundadores Sebastián Silva y Alfredo Gutiérrez a coordinar acciones directamente con otros voluntarios en Arequipa, Puno y La Paz.

También en 2008 se une al equipo el profesor Koke Contreras quien puso su experiencia y énfasis en el acompañamiento pedagógico que sería necesario para la apropiación.

En 2009 integran el equipo Juan Camilo Lema y Laura Victoria Vargas, impulsando el primer proyecto mayor, así como la adopción del nombre **Equipo de I+D SomosAzucar.Org**.

En 2008-2010 varios extranjeros colaboran con el equipo en diversas actividades, Kaisa Haverinnen y Tuuka Hastrup de Finlandia, Antje Breitkopf de Alemania, Rubén Rodríguez de Cataluña. Localmente destacó el equipo independiente en Puno: Rubén Monrroy, David Cruz, Neyder Achahuanco.

En 2011 sería significativa la visita de Aleksey Lim, de la Federación Rusa, quien implementó el servidor y base de datos distribuída de la Red Azúcar, la cual fue modelada para satisfacer la necesidad de acompañamiento pedagógico detectada.

También hemos colaborado con Ignacio Rodríguez, Cristian García y Flavio Danesse, de Uruguay, quienes apoyaron nuestro taller de videojuegos, presencialmente, en el evento "Sugar Camp Chía 2014" realizado en Colombia.

_¿CÓMO LO HIZO?_


_¿DÓNDE LO HIZO?_
El despliegue oficial de Hexoquinasa 2 empezó en 2014 y a la fecha de redacción (Oct 2017), más de 40.000 laptops únicas han conectado con la Red Azúcar y continúan apareciendo nuevas en todo el territorio peruano.

# _Desarrollo_
_¿QUÉ OBTUVO?_
Uno pensaría que nuestro equipo cuenta con prestigio internacional gracias a la sostenida contribución que hacemos al bien común tecnológico llamado Sugar.

El impacto de _Hexoquinasa_ y la _Red Azúcar_ en la práctica se ha visto limitado por el uso de DRM en las laptops del proyecto OLPC, sistema de "grilletes digitales" (Stallman), mediante el cual el fabricante eficazmente impide la instalación de Software no autorizado.

Siendo el proyecto Hexoquinasa un proyecto de cierta complejidad técnica, pero de alcance local, no logramos motivar a la comunidad _upstream_ para tomar en cuenta las ideas subyacentes o apoyar al equipo en la implementación y sustentación del proyecto. Sin embargo las contribuciones en materia de lenguas nativas, sí fueron adoptadas, posibilitando en adelante las traducciones a estas lenguas en todos los sistemas operativos libres.



# _Conclusión_
_¿QUÉ VENTAJAS ENCONTRÓ?_

El valor del trabajo de los voluntarios en los despliegues de tecnología educativa es con frecuencia infraestimado.
