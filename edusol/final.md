# Resumen



Pero que determina la calidad de un software?

La calidad de cualquier solución estará parcialmente determinada por la capacidad de la solución de facilitar los procesos naturales que derivan de la madurez en el uso de los usuarios.

No existe un standar internacional para asegurar la calidad del Software para niños [Referencia]. 

En el jardín de flores del Software Libre, entre los años 2007 y 2010, el proyecto (r) SUGAR Labs /Laboratorios AZUCAR floreció alcanzado a educandos en Perú, Uruguay, Colombia, México y Nicaragua [Refencias].

En menos de un quinquenio, este software compartía los espacios de aprendizaje de niños, niñas y maestros por toda latinoamérica y otras regiones del mundo. 

SUGAR/AZUCAR era una solución bien intensionada con un diseño metafórico lógico y minimalista que respondia a la necesidad de estandarizar la presentación de mas de 300 Actividades libres disponibles para los aprendices.

Pero su calidad había sido concebida en una realidad anglosajona a un ritmo acelerado y ajeno a la realidad de sus usuarios. Una versión desplegada en terreno, como cualquier otra solución desplegada en terreno, requeriría soporte técnico, documentación, actualizaciones y retroalimentación por parte de un equipo neutral e independiente. 

Este espacio estuvo vacio por muchos años. Infinitos y apasionados debates sobre el impacto del uso de la tecnología SUGAR apaciguaban los ánimos y estancaron el diseño.

Hoy 10 años mas tarde SUGAR se prepara para una evolución escalonada, sustentada en los resultados de trabajo de campo y las recomendaciones de todos aquellos que se han tomado el esfuerzo de hacer mediciones en el incremento de 


Que este proyecto evolucione a favor del bienestar de los educandos es una oportunidad cultural inmensa. 

Sin embargo, identificamos desafíos técnico-sociales importantes;

[1] Asegurar el cumplimiento de Códigos de Conducta que permitan una participación multicultural neutral multilingue en los procesos para alcanzar las decisiones de diseño.

[2] 

[3]
Nuestro equipo de base realizó un llamado los resultados de las intervenciones hechas al diseño de SUGAR y producción de imagnes de sistema operativo "customizadas", localizadas, guías de programación, guías de traducción, video-juegos "transformadores" y millas y millas de Educación en Casa-Taller Q2 gracias al software libre.

son los linemientos de diseño y códigos de conducta sean ejemplares.

y  mas estrictos será el software  que por diseño esta orientado a este segmento de la población que es vulnerable y dependiente del mundo de los "adultos".  

El software es entendido por nuestro equipo como un proceso sistémico cuyo estado de "salud" en función del tiempo puede ser determinado [Variables-Sugar-Network].

Estado de salud del sistema = variable 1 (t),variable 2 (t),variable 3 (t)

sintetizarse 

Antecedentes

Sebastian y Laura se conocieron el Día del Software Libre (SFD) 19 de Septiembre de 2009. 

Aquel día Sebastian presentaba la conferencia “Pedagogía del Oprimido – Software de la Liberación” mientras que Laura, escuchaba atenta aquel mensaje liberador; era la llamada a la ética y a la acción. La invitación animaba a la sociedad civil a entender la necesidad de apropiarse del software libre llamado "SUGAR" que sería distribuido a millones de niños en los años siguientes en los "países en vías de desarrollo". 

SomosAzucar.Org

En 2008 Sebastian organizó los primeros voluntarios que se propusieron apoyar el despliegue de estas computadoras portátiles en Perú, reuniendo inicialmente una lista de 120 personas. El choque fué estrepitoso, pues los voluntarios no encontraron espacio en el marco de trabajo planteado entre el fabricante de las máquinas laptop XO (One Laptop Per Child) y el Ministerio de Educación de Perú. 

Para la sociedad civil, las puertas a la colaboración estaban cerradas. 

La mayor preocupación ante esta situación, era que la imagen de sistema operativo instalada en terreno era una imagen de pruebas y estaba llena de errores y frustación para aprendices y maestros. 

Así entonces, nace SomosAZUCAR.Org como equipo de investigación y desarrollo desde donde formulamos múltiples estrategias para dar soporte a una imagen de sistema operativo actualizada que mejore la experiencia de uso del sistema SUGAR/AZUCAR en Castellano, Quechua y Aymara.

¿QUÉ HIZO?

En Perú, desde 2014 se instala en las Laptops XO 1.5 "Hexoquinasa", una edición especial de SUGAR/AZUCAR producida por el equipo SomosAZUCAR.Org basada en SUGAR 0.94 con acceso directo desde la interfase a una red experimental de aprendizaje por proyectos.

Sin embargo, se ha evidenciado que las barreras para llevar estos avances al proyecto 

El resultado de ocho años de práctica (investigación, diseño y desarrollo) se materializan hoy en una imagen de sistema operativo que hemos bautizado AZUCAR-libre [check tecnico] actualizada y disponible para los aprendices de habla Castellana, con la novedad de tener como ícono central, un corazón alado.


Si bien es cierto que estamos lejos aún de un entorno inclusivo y colaborativo de aprendizaje para niños, es importante subrayar que se ha dado un paso firme con la disponibilización de una versión de AZUCAR-Libre de Trademarks en los paquetes de Arte, especialmente liberando el ícono central de la experiencia de usuario.

¿CON QUIÉN LO HIZO?

En contra de la 

¿CÓMO LO HIZO?


¿DÓNDE LO HIZO?

¿QUÉ OBTUVO? 

¿QUÉ VENTAJAS ENCONTRÓ?


Hoy, mientras educamos a nuestros dos hijos en casa con la ayuda de Software apropiado, hacemos un balance positivo de nuestros aprendizajes. 


Rápidamente las lenguas nativas tomaron prioridad y así formulamos una estrategia para lograrlas; nuestro primer evento fue una Maratón de Traducción; "Miski Pachamama 2010". A orillas del lago Titicaca y con el apoyo de la sociedad civil movilizada ante el llamado del Software Libre, se logró completar el 60% de las traducciones al Quechua y al Aimara de "Sugar".

Pero las traducciones no eran suficientes. Se requería movilizar _hackers_ de todo el mundo para responder ante el reto técnico de construir una imagen de sistema operativo actualizada y optimizada para la experiencia local peruana. Esta sería nuestra oportunidad de tener un impacto positivo y transmitir nuestro mensaje. 

Convocamos a la comunidad global para Sugar Camp Lima 2011 y vino gente de varios países del mundo y también desde Puno, para colaborar en este proceso. Para ese entonces el Ministerio de Educación despertó a la necesidad de estas lenguas en la interfase de usuario y viendo que había un trabajo ya avanzado, nos contactó para que los asesoráramos en la metodología para llevar a cabo esta traducción e incluir estas traducciones en imágenes instalables de sistema operativo, para su distribución a nivel nacional.

Desarrollaríamos una imagen de sistema operativo actualizada que incluyera un mecanismo de comunicación descentralizado para permitir que educadores, padres, niños, desarrolladores y voluntarios tuvieran un diálogo directo.

Así nació la primera relación de colaboración entre los voluntarios y el Ministerio de Educación de Perú. 

En 2014 el Ministerio de Educación empezó el despliegue de "Hexoquinasa" en terreno - que ellos llamaron "Sugar 9" - nuestra imagen de sistema operativo actualizada, que incluía Quechua y Aymara.

Por estos tiempos los niveles de actividad en las listas de correo de Sugar iban en declive así el ritmo de desarrollo. Sugar había llegado a planicie y no habiendo sido apropiado por ninguna comunidad excepto sus propios creadores, cayó en un estancamiento en cuanto a su evolución. A nivel global, no solamente no se hicieron mejoras, sino que se descuidó el código hasta el punto que muchas de los paquetes dependen de bibliotecas obsoletas, lo cual amenaza con sacar a Sugar de Debian y otras distribuciones.

3 años despues, al menos 40,000 máquinas fueron actualizadas en terreno con Hexoquinasa. 

Sugar es software libre para niños y tenemos la esperanza de que con perseverancia lograremos desembarazar a Sugar del karma que le dejó el programa Una Laptop Por Niño". Nuestra tarea es titánica y nos exige insistir e insitir en la eliminación del logo ícono de XO marca registrada como ícono principal de la interfase de Sugar.

Si para romper con las prácticas que inhiben el uso de Sugar por más despliegues y fabricantes de hardware, será necesario abrir el desarrollo a un proyecto aparte (_fork_), esto está aún por verse. Sea como sea el final de esta historia, gracias al amor, los niños hoy ya cuentan con Sugar 0.111-libre.
