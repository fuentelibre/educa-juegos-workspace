 * Pelecanidae - pelícanos;

  * Sulidae - alcatraces;

  * Phalacrocoracidae - cormoranes;

  * Fregatidae - fragatas;

  * Anhingidae - aves serpiente;

  * Phaethontidae - rabijuncos;

 * Superfamilia *Psittacoidea* (*loros típicos*):

  * Familia Psittacidae

   * Subfamilia Psittacinae: dos géneros africanos, Psittacus y Poicephalus.

  * Subfamilia Arinae, loros neotropicales:

  * Tribu Arini: 15 géneros de guacamayos y afines.

  * Tribu Androglossini: 7 géneros de loros 
neotropicales de cola corta.

  * Incertae sedis: 10 géneros.

  * Pionites (2 especies)

  * Deroptyus (1 especie, el loro cacique)

  * Hapalopsittaca (4 especies)

  * Nannopsittaca (2 especies)

  * Psilopsiagon (2 especies, anteriormente en Bolborhynchus)

  * Bolborhynchus (3 especies)

  * Touit (8 especies)

  * Brotogeris (8 especies)

  * Myiopsitta (1 o 2 especies)

  * Forpus (7 especies)

  * Familia Psittrichasiidae

  * Subfamilia Psittrichasinae: una especie, el loro aguileño.

  * Subfamilia Coracopsinae: un género con dos especies vivas de Madagascar e islas circundantes.

  * Familia Psittaculidae

  * Subfamilia Platycercinae

  * Tribu Pezoporini: pericos terrestres y afines.

  * Tribu Platycercini: pericos de cola ancha.

  * Subfamilia Psittacellinae: 1 género (Psittacella) con varias especies.

  * Subfamilia Loriinae

  * Tribu Loriini: loris.

  * Tribu Melopsittacini: una especie, el periquito común.

  * Tribu Cyclopsittini: loritos de higuera.

  * Subfamilia Agapornithinae: 3 géneros de inseparables y afines.

  * Subfamilia Psittaculinae

  * Tribu Polytelini: 3 géneros de papagayos y pericos de Australasia.

  * Tribu Psittaculini: loros asiáticos.

  * Tribu Micropsittini: microloros.

 * Strigidae

 * Tytonidae

 * Apodidae

 * Hemiprocnidae

 * Trochilidae

 * Aegothelidae

* Familia Lybiidae - barbudos de África (unas 40 especies, segregados recientemente de Capitonidae).

Familia Megalaimidae - barbudos de Asia (unas 25 especies, segregados recientemente de Capitonidae).

Familia Ramphastidae - tucanes, tucanetes y arasaríes (unas 40 especies).

Familia Semnornithidae - tucanes barbudos (2 especies, segregados recientemente de Capitonidae).

Familia Capitonidae - barbudos, barbuditos y cabezones de América (unas 15 especies).

Familia Miopiconidae (extinta).

Familia Picavidae3​ (extinta).

Familia Picidae - carpinteros, carpinteritos, chupasavias, picamaderos, picatroncos, picos, pitos y torcecuellos (unas 200 especies).

Familia Indicatoridae - indicadores (17 especies).
Todidae - barrancolíes, cartacubas y sampedritos
Momotidae - momotos, barranqueros o guardabarrancos
Alcedinidae - martines pescadores
Halcyonidae - martines cazadores, cucaburras y alciones
Cerylidae - martines gigantes
Meropidae - abejarucos
Coraciidae - carracas
Brachypteraciidae - carracas terrestres
Leptosomatidae - carraca curol
Upupidae - abubillas
Phoeniculidae - abubillas arbóreas
Bucerotidae - cálaos