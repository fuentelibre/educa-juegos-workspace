## Trogonidae

Los **trogónidos** (**Trogonidae**) son la única familia del orden de los **Trogoniformes**. Engloba a seis géneros: Apaloderma, Pharomachrus (quetzales), Euptilotis, Priotelus,  ![trogon](https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Trogon-viridis-002.jpg/250px-Trogon-viridis-002.jpg)Trogon (también llamados **sucurúas** o **surucuaes**) y Harpactes.

La palabra griega **trogon** significa «mordisquear» y se refiere al hecho que estas aves horadan agujeros en los árboles o en termiteros para construir sus nidos.

Los trogones son residentes de los bosques tropicales de todo el mundo, con la mayor diversidad en América Central y América del Sur. El género Apaloderma contiene las pocas especies originarias de regiones afrotropicales, y el género Harpactes es originario de Asia, más concretamente en las regiones indo-malayas. El resto de especies pertenecen a las regiones neotropicales.

Se alimentan de insectos, frutos y plantas, generalmente de la familia Lauracea, Theaceae, Myrsinaceae, Araliaceae, Verbenaceae, Solanaceae, Myrtaceae, Melasomataccae, Moraceae o Clusiaceae. Sus picos anchos y patas débiles reflejan su dieta y los hábitos arbóreos. Aunque su vuelo es rápido, estas aves son reticentes a volar cualquier distancia. Los trogones no emigran, a menos que haya escasez de alimentos.

Tienen un plumaje suave, a menudo muy colorido, con un marcado dimorfismo sexual. Algunos tienen un paladar esquizognato, algunos procesos basipterigoides, un gran vomer, 15 vértebras cervicales, cuatro muescas esternales profundas, de cuatro a cinco pares. Ponen huevos de color blanco o ligeramente pastel, y las crías tienden a tener una piel rosada, con características adicionales que varían dependiendo de la especie.


|Apaloderma vttatum|
|------------------|
|Trogón de cola barreada|

<font color='pink'>longitut:</font>28-30cm


<font color='pink'>peso:</font>
https://upload.wikimedia.org/wikipedia/commons/thumb/4/49!/Description_des_reptiles_nouveaux%2C_ou%2C_Imparfaitement_connus_de_la_collection_du_Mus%C3%A9um_d%27histoire_naturelle_et_remarques_sur_la_classification_et_les_caract%C3%A8res_des_reptiles_%281852%29_%28Crocodylus_moreletii%29.jpg/1024px-thumbnail.jpg