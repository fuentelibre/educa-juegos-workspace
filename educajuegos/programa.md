# Escribiendo con MarkDown

_MarkDown_ es un código para escribir Documentos utilizando las tecnologías Web.

El `código` es lenguaje interpretado por la computadora.

En código _MarkDown_ se pueden hacer encabezados, **negritas**, _cursivas_, __subrayadas__, y también listas:

```markdown
# Encabezado nivel 2

* **negrita**
* _cursiva_

(lo anterior era una lista)
```

Es código MarkDown que una vez interpretado resulta en el siguiente documento:

> ## Encabezado nivel 2
>
> * **negrita**
> * _cursiva_
>
> (lo anterior era una lista)

Obsérvese que el código es _texto_. En este caso el código le indica a la computadora cómo debe dibujar las letras en la pantalla.

# 👶 👅 👓 📖 💻

A diferencia del lenguaje humano, el código es _interpretado por la computadora_.

Ella tiene una comprensión muy limitada de lo que sucede.