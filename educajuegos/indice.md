# Indice

## Base

1. Usando Jappy:
    1. Editor, pestañas, archivos

2. Texto en formato Markdown
    1. Encabezados, párrafos, énfasis, listas
    2. Tablas, enlaces, imágenes
    
3. HTML
    1. Atributos
    2. Estilos

4. CSS

## Programar

1. Python

2. Javascript