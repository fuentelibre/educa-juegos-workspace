# Programando el *Navegador Web 1*

# HTML

## Los elementos HTML.

Existen diferentes elementos HTML. Los elementos HTML se expresan demarcando un comienzo y un final con una etiqueta, por ejemplo, `button`:

```html
<button>Soy un botón</button>
```

La etiqueta `button` designa un elemento de tipo botón. Cuando el navegador encuentra un elemento válido (con comienzo y fin), lo dibuja o renderiza en la pantalla, así:

> <button>Soy un botón</button>

## Los atributos de los elementos

Los elementos pueden tener atributos. Por ejemplo:

```html
<button disabled="true">Soy un botón desactivado</button>
<input size="20" maxlength="4" placeholder="Maximo 4"></input>
```
> <button disabled=true>Soy un botón desactivado</button>
> <input size="20" maxlength="4" placeholder="Maximo 4"></input>

## Los estilos de los elementos

Un atributo muy importante de todos los elementos HTML es el atributo _style_ que quiere decir "estilo".

```
<button style="background-color: Pink; 
    font-size: 30pt; border-radius: 20px;
    padding: 30px">Soy un botón hermoso</button>
```
> <button style="background-color: Pink; font-size: 30pt; border-radius: 20px; padding: 30px">Soy un botón hermoso</button>

