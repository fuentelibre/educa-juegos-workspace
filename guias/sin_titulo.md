
# Usando P5

(Es necesario tener el archivo `p5.min.js`)

```
import p5

def setup():
    createCanvas(480, 500)
    background(200, 70, 70)
    noStroke()

def draw():
    if (mouseIsPressed):
        stroke(255)
    else:
        fill("DeepPink")
        noStroke()
    triangle(
         mouseX, mouseY, 
         mouseX + 80, mouseY + 80,
         mouseX - 100, mouseY + 100)
```