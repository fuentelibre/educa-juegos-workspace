Erythroxylum coca11

Valor nutricional por cada 100 g

Energía 73 kcal 305 kJ

Carbohidratos	46.2 g

• Fibra alimentaria	14.4 g

Grasas	5.0 g

Proteínas	18.9 g

Agua	6.5 g

Retinol (vit. A)	6598.68 μg (733%)

Tiamina (vit. B1)	0.35 mg (27%)

Riboflavina (vit. B2)	1.91 mg (127%)

Niacina (vit. B3)	1.3 mg (9%)

Ácido pantoténico (vit. B5)	0.685 mg (14%)

Vitamina B6	0.508 mg (39%)

Ácido fólico (vit. B9)	0.13 μg (0%)

Vitamina C	1.4 mg (2%)

Vitamina E	29 mg (193%)

Calcio	1540 mg (154%)

Cobre	1.21 mg (0%)

Hierro	45.8 mg (366%)

Magnesio	213 mg (58%)

Manganeso	6.65 mg (333%)

Fósforo	911 mg (130%)

Potasio	2.02 mg (0%)

Sodio	40.6 mg (3%)

Zinc	2.7 mg (27%)

% de la cantidad diaria recomendada para adultos.