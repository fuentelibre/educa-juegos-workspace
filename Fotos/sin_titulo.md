 * Mara, dios de la destrucción

 * Mara, señor de la ilusión, líder de los demonios

 * Mara, un demonio

 * Mara, parte negativa que habita en cada persona y construye una ilusión del interior de la propia mente.

 * Mara, las contaminaciones mentales de cada individuo, las mismas que podríamos denominar "ignorancia"; y es ésta misma la que impide a los seres lograr el Nirvana.