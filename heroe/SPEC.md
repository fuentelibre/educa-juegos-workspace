# "Heroe" (Videojuego)

# Concepto

 - "Correr, Saltar, Escalar"
 - "Editar el Entorno (lógica / definición / mapa)"
 - "Controlar con Controlador de Videjuegos"

# Personaje

![](images/obj_Run000.png)

## Animaciones disponibles

 * Saltar
 * Correr
 * Caminar
 * Sentar
 * Disparar (pistola)
 * Disparar (uzi)
 * Dar estocada
 * Caer
 * Golpear
 * Dormir