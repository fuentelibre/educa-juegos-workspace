# MoviePy

[Moviepy](https://zulko.github.io/moviepy/)

MoviePy es un módulo de Python para la edición de video, el cual puede ser usado para realizar operaciones básicas (tales como cortar, pegar, e insertar títulos), composición de video (también conocido como "edición no-lineal"), procesamiento de video, o para crear efectos avanzados. Puede leer y escribir los formatos más comunes de video, incluyendo GIF.

Aquí se le puede ver en acción:

![Pantallazo](https://zulko.github.io/moviepy/_images/demo_preview1.jpeg)