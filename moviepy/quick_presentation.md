# Pequeña presentación

Esta sección explica en qué casos puede usarse MoviePy y como funciona.

## ¿Necesito MoviePy?

Aquí hay algunas razones por las cuales podrías desear editar videos en Python:

* Tienes muchos videos para procesar o componer de una forma compleja
* Deseas automatizar la creación de videos o animaciones GIF en un servidor (Django, Flask, etc.)
* Deseas automatizar tareas tediosas, tales como la inserción de títulos, el seguimiento de objetos, el recorte de escenas, la creación de créditos finales, subtítulos, etc.
* Deseas programar tus propios efectos de video para realizar algo que no se puede lograr con ningún editor de video.
* Deseas crear animaciones a partir de imágenes creadas por otra biblioteca de Python (Matplotlib, Mayavi, Gize, scikit-images...)

Y aquí hay algunos usos para los cuales MoviePy NO es la mejor solución:

* Si necesitas hacer análisis de video cuadro a cuadro (con detección de caras por ejemplo). Esto podría hacerse con MoviePy en asociación con otra biblioteca, sin embargo, aconsejamos utilizar [imageio](https://imageio.github.io/), [OpenCV](http://opencv.org/) o SimpleCV, bibliotecas especializadas para estas tareas.
* Si solo deseas convertir un archivo de video, o una serie de imágenes en un video. En este caso, es mejor utilizar directamente `ffmpeg` (o `avconv` o `mencoder`...) porque será más rápido y eficiente que programarlo en MoviePy.

## Ventajas y limitaciones

MoviePy ha sido desarrollado con los siguientes objetivos en mente:

* **Que sea simple e intuitivo**. Operaciones básicas se hacer con un comando. El código es fácil de aprender y comprender por novatos.
* **Flexible**. Tienes control total sobre los cuadros del video y el audio, y crear tus efectos es muy fácil.
* **Portátil**. El código usa software muy común (Numpy y FFMPEG) y puede ejecutarse en (casi) cualquier máquina con (casi) cualquier versión de Python.

Ahora, para las limitaciones: MoviePy no puede (aún) transmitir videos (leer de una cámara, o enviar el video en vivo hacia una computadora remota), y no está diseñado para el procesamiento que involucre muchos cuadros sucesivos en una película (tales como estabilización de video, necesitarás otro software para ello). También podrías llegar a tener problemas de memoria si utilizas demasiadas fuentes de video, audio e imágenes al mismo tiempo (>100), pero esto será solucionado en versiones futuras.


