from flask import Flask
app = Flask(__name__)

@app.route('/')
def homepage():
    return """
    Hello world!
    This is my first web app!
    """

if __name__ == '__main__':
    app.run(use_reloader=True)


    