# Jappy ![](/activity/jappy.svg)


# <font color='blue'>TODO</font> (pendientes)
 - Ejemplo: Adivinanzas
 - Ejemplo: Memorize (puntaje)
 - Subir ejemplos de sugarizer (clonar desde git)
 - Reportar tiempo compilando
 - Arrastrar para redimensionar la división
 - Formato Javascript (.js)
 - Modo Vim :-)
 - Compilar .pyj -> .js / .md -> .html
 - Íconos en notificaciones
 - Sincronización de *scroll* (modo markdown)
 - Colorizado de sintaxis (modo markdown, 
    reutilizar [las del editor](http://codemirror.net/demo/runmode.html))

## <font color='red'>BUGS</font> (fallas)

 - Determinar ruta de acción cuando des-sincroniza
 - Al cambiar de nombre a la pestaña la sesión compartida no elimina el nombre anterior sino que abre uno nuevo nomás.
 - Notificaciones múltiples aparecen una sobre otras
 
[Reportar](https://github.com/somosazucar/Jappy/issues/new)

## <font color='green'>DONE</font> (tareas completadas)

 - Búsqueda en el editor
 - Notificaciones de eventos ("Toasts")
 - Añadir soporte para otros formatos de archivos:
   - Imágenes (.png, .jpg, .svg)
   - HTML directo (.html)
   - Texto Markdown (.md)
 - <strike>Intenta abrir imágenes en el editor al restaurar la sesión</strike>
 - Internacionalización
   - Español
 - <strike>Al cambiar el modo se pierde la posición del cursor</strike>. Efecto de la *colaboración*, resuelto.

![](cutesy-animals.png)