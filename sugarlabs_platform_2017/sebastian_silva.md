# Vote por Sebastian Silva
## Para la Junta de Supervisión de Sugar Labs

La misión de la Junta de Supervisión de Sugar Labs es velar por que la comunidad tenga claridad de propósito y los medios para alcanzar sus objetivos.

Sugar Labs necesita reconocer que nuestra comunidad es **diversa**. Por tanto existen diversos propósitos que pueden ser claros y a la vez opuestos.

Deberíamos proveer un **espacio seguro y neutral** para dialogar sobre la tecnología y educación que subyace a nuestros proyectos. Necesitamos aprobar y hacer valer un *código de conducta*.

Finalmente, la razón de ser existir Sugar Labs bajo el alero de la SFC es **facilitar el flujo de recursos** a voluntarios de la comunidad con proyectos valiosos. Deberíamos velar por abrir y viabilizar convocatorias con fondos para desarrollo, infrastructura y productos de usuario final.

## Acerca de mí

Sebastian es padre de dos preciosos niños los cuales son educados en casa en co-laboración con su querida esposa y co-creadora Laura Vargas.

De niño, Sebastián aprendió a programar con Logo y pronto lo superó para encantarse con Python y levantar la bandera del Software Libre. Le encanta escribir Actividades para Sugar; La última es una IDE de Python colaborativa que él espera que también les guste.

En la actualidad vive con su familia en una cabaña en la selva del Amazonas y enseña Karate y programación de videojuegos a los niños de la zona, sosteniendo un estilo de vida que busca la armonía con la naturaleza y prestando servicios de artesanía de software y jardinería de sistemas a distancia.