# Vote for Sebastian Silva
## To join the Sugar Labs Oversight Board

The mission of the Sugar Labs Oversight Board is to ensure the community has clarity of purpose and the means to attain its goals.

Sugar Labs needs to recognize that our community is **diverse**. Therefore, there are diverse purposes that may be clear and at the same time opposed.

We should provide a **safe and neutral space** for dialogue on the technology and education that underlies our projects. We need to approve and enforce a *code of conduct*.

Finally, the reason for Sugar Labs to exist under the SFC is to **facilitate the flow of resources** to volunteers with valuable projects. We should strive to open and fund open calls with funds for development, infrastructure and end user products.

## About me

Sebastian is father to two precious children that are being homeschooled in collaboration with his dear wife and co-creator Laura Vargas.

As a child, Sebastian learned to program with Logo and soon outgrew it to be charmed with Python and raise the flag of Free/Libre Software. He loves writing Sugar Activities; His latest is a collaborative Python IDE that he hopes you'll love too.

Currently he lives with his family in a cabin in the Amazon rainforest and teaches Karate and videogame programming to the kids in the vecinity, sustaining a lifestyle seeking harmony with nature and providing services of software artisanship and systems gardening remotely.